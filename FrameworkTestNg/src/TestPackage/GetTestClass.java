package TestPackage;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Api_Common_Methods.CommonMethodsHandleApi;
import Endpoints.GetEndpoint;
import UtilityCommonMethods.ManageApiLogs;
import UtilityCommonMethods.ManageDirectory;

public class GetTestClass extends CommonMethodsHandleApi {
	static File LogDir;
	static String endpoint;
	static String responsebody;
	@BeforeTest
	public static void TestSetUp() {
		LogDir = ManageDirectory.CreateLogDirectory("GetTestClassLogs");
		endpoint = GetEndpoint.GetEndpointTc1();
	}
	@Test
	public static void Get_executor() throws IOException {
		

		for (int i = 0; i < 5; i++) {
			int statuscode = get_statuscode(endpoint);
			System.out.println(statuscode);
			if (statuscode == 200) {
				responsebody = get_responsebody(endpoint);
				System.out.println(responsebody);				
				GetTestClass.validator(responsebody);
				break;
			} else {
				System.out.println("Expected statuscode not found,hence retrying");
			}
		}
	}

	public static void validator(String responsebody) {
		JSONObject array = new JSONObject(responsebody);
		JSONArray res_array = array.getJSONArray("data");

		int id[] = { 7, 8, 9, 10, 11, 12 };
		String firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };

		int count = res_array.length();
		for (int i = 0; i < count; i++) {
			int exp_id = id[i];
			String exp_firstname = firstname[i];
			String exp_lastname = lastname[i];
			String exp_email = email[i];

			int res_id = res_array.getJSONObject(i).getInt("id");
			String res_firstname = res_array.getJSONObject(i).getString("first_name");
			String res_lastname = res_array.getJSONObject(i).getString("last_name");
			String res_email = res_array.getJSONObject(i).getString("email");

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
			Assert.assertEquals(res_email, exp_email);
		}
	}
	@AfterTest
	public static void TestTearDowm() throws IOException {
		ManageApiLogs.evidence_creator(LogDir,"GetTestClass1", endpoint,"GET requests don't have Requestbody.", responsebody);
	}

}
