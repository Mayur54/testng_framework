package TestClassTestngFileParameterization;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Api_Common_Methods.CommonMethodsHandleApi;
import Endpoints.PostEndpoint;
import UtilityCommonMethods.ManageApiLogs;
import UtilityCommonMethods.ManageDirectory;
import io.restassured.path.json.JsonPath;

public class PostTestClassParameters extends CommonMethodsHandleApi {
	static File LogDirectory;
	static String requestbody;
	static String endpoint;
	static String responsebody;
	@BeforeTest
	public static void TestSetUp() throws IOException {
		LogDirectory = ManageDirectory.CreateLogDirectory("PostTestClassLogs");	
		endpoint = PostEndpoint.PostEndpointTc1();
	}
	@Parameters({"Req_name","Req_job"})
	@Test()
	public static void Post_executor(String Req_name,String Req_job) throws IOException {
		requestbody = "{\r\n"
				+ "    \"name\": \""+Req_name+"\",\r\n"
				+ "    \"job\": \""+Req_job+"\"\r\n"
				+ "}";
		for (int i = 0; i < 5; i++) 
		{
			int statuscode = post_statusCode(requestbody, endpoint);
			System.out.println(statuscode);

			if (statuscode == 201)
			{
				responsebody = post_responsebody(requestbody, endpoint);
				System.out.println(responsebody);
				ManageApiLogs.evidence_creator(LogDirectory,"PostTestClass1", endpoint, requestbody, responsebody);
				PostTestClassParameters.validator(requestbody, responsebody);
				break;
			} 
			else 
			{
				System.out.println("Expected statuscode not found,hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String exp_date = currentdate.toString().substring(0, 6);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_date = jsp_res.getString("createdAt").substring(0,6);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(exp_date, res_date);
		Assert.assertNotNull(res_id);
	}
	@AfterTest
	public static void TestTearDown() throws IOException {
		ManageApiLogs.evidence_creator(LogDirectory,"PostTestClass1", endpoint, requestbody, responsebody);
	}
}