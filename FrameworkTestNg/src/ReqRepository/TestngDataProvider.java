package ReqRepository;

import org.testng.annotations.DataProvider;

public class TestngDataProvider {
	@DataProvider()
	public Object[][] PostDataProvider() {
		return new Object[][]
				{
			       {"mihir","civil"},
			       {"sakshee","Engg"},
			       {"samarth","CompEng"}
				};
	}
	@DataProvider()
	public Object[][] PutDataProvider() {
		return new Object[][]
				{
			{"Aish","ba"},
			{"Kunal","ca"},
			{"vishal","ma"}
				};
	}
	@DataProvider
	public Object[][] PatchDataProvider() {
		return new Object[][]
				{
			{"Shweta","Qa"},
			{"Nupur","Srqa"}
			
				};
	}

}
