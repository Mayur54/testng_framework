# TestNG_Framework


Welcome to the TestNG Framework ! This framework is designed for efficient testing using TestNG, encompassing various annotations, data-driven testing, and common methods for handling status codes and response bodies. It supports both parallel and serial testing.




**Features:**

- Annotation Variety: Utilize different types of TestNG annotations for test lifecycle management.

- CommonMethods Package: Centralize status code and response body handling for consistent test execution.

- Data-Driven Testing:Leverage the @DataProvider annotation for data-driven testing.

- Parameters:To pass data from xml by unsing parameter tag in xml and @parameters annotation.

- Parallel Testing: Execute tests in parallel for faster test suite execution.

- Serial Testing: Opt for serial testing when needed for specific scenarios.

In this Framework we have libraries which we are using , they are :
1) RestAssured to triger the api ,extract the responebody and status code,
2) JsonPath To parse the responsebody,
3) Apache-Poi to read and write from an excel file, 
4) TestNG libraries for validating responsebody parameters by using assert.
